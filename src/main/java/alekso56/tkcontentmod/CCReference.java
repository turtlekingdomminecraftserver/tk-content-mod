package alekso56.tkcontentmod;

import java.util.LinkedList;
import java.util.List;

import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import alekso56.tkcontentmod.turtles.TKTurtlePeripheral;
import alekso56.tkcontentmod.turtles.uselesswrapper;
import cpw.mods.fml.common.registry.GameRegistry;
import dan200.computercraft.api.ComputerCraftAPI;
import dan200.computercraft.api.turtle.ITurtleUpgrade;

public class CCReference {

	static final List<ITurtleUpgrade> upgrades = new LinkedList<ITurtleUpgrade>();
	
	static void Init(){
	    ComputerCraftAPI.registerPeripheralProvider(new uselesswrapper());
		ComputerCraftAPI.registerTurtleUpgrade(new TKTurtlePeripheral());
	}
	
	static void addUpgrades(List list){
			ItemStack base = GameRegistry.findItemStack("ComputerCraft", "CC-TurtleExpanded", 1);
			if (base != null) {
				for (ITurtleUpgrade upgrade : upgrades) {
					ItemStack upg1 = base.copy();
					upg1.stackTagCompound = new NBTTagCompound();
					upg1.stackTagCompound.setShort("leftUpgrade", (short) upgrade.getUpgradeID());
					list.add(upg1);
				}
			}
			ItemStack base2 = GameRegistry.findItemStack("ComputerCraft", "CC-TurtleAdvanced", 1);
			if (base2 == null)
				return;
			for (ITurtleUpgrade upgrade : upgrades) {
				ItemStack upg1 = base2.copy();
				upg1.stackTagCompound = new NBTTagCompound();
				upg1.stackTagCompound.setShort("leftUpgrade", (short) upgrade.getUpgradeID());
				list.add(upg1);
			}
	}
	
	static void fetchAndAdd(int wot){
		ITurtleUpgrade upgrade = upgrades.get(wot);
		if(upgrade != null){
		 //add some recipe ifs or switches if you add more upgrades later.
		 GameRegistry.addShapedRecipe(upgrade.getCraftingItem(),"xxx","xyx","xxx",'x',Blocks.stone,'y',Blocks.tnt);
		}
	}
}
