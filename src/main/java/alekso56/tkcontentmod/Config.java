package alekso56.tkcontentmod;

import net.minecraftforge.common.config.Configuration;
import scala.Int;

public class Config {
	public static boolean mainMenuReplacement,FetchFromInternet,turtleselfdestruct,python,disableEnderPearls,ExtraRecipesCC,blockFriendlySelfdestruct,noParticlesSelfdestruct,AutoLaunch;
	public static int margins,explosionSize,selfdestructFuelRequirement;
	public static String bookURL,buildNum,menuString;
	public static void loadConfig(Configuration config) {
		config.load();
		mainMenuReplacement = config.get("Menus", "replaceMenus", true).getBoolean(true);
		FetchFromInternet = config.getBoolean("ffi","Menus",false,"Fetch build.txt from internet");
		bookURL = config.get("Menus", "ffiURL", "http://examplepage.domain/build.txt").getString();
		buildNum = config.get("Menus", "buildNum", "23.5").getString();
		menuString= config.get("Menus", "menuString", "TurtleKingdom").getString();
		margins = config.getInt("margins", "Menus", 4, 0, 400, "Margins between buttons on menus");
		turtleselfdestruct = config.getBoolean("selfdestruct","Peripherals",true,"Enable selfdestruct at all");
		blockFriendlySelfdestruct = config.getBoolean("noBlockDmg", "Peripherals", true, "Do no block damage on selfdestruct, just drop the turtle");
		noParticlesSelfdestruct = config.getBoolean("noParticles", "Peripherals", true, "Spawn no particles on selfdestruct");
		selfdestructFuelRequirement = config.getInt("selfdestructRequirement", "Peripherals", 1, 0, Int.MaxValue(), "Amount of fuel required to selfdestruct");
		python = config.getBoolean("python","Peripherals",false,"Enable python in special peripheral");
		AutoLaunch = config.getBoolean("AutoLaunch", "Peripherals", true, "Enable autolaunch on blockplacement (Launcher block)");
		disableEnderPearls = config.getBoolean("noThrowender","General",false,"Disable EnderPearl Teleportation");
		ExtraRecipesCC = config.getBoolean("recipesCC","General",true, "Enable extra recipes for ComputerCraft (Cover everything with goldingots to upgrade)");
		config.save();
	}

}
