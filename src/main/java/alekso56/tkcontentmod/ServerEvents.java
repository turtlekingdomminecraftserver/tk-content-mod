package alekso56.tkcontentmod;

import net.minecraft.init.Items;
import net.minecraftforge.event.entity.player.PlayerUseItemEvent;

import com.google.common.eventbus.Subscribe;

public class ServerEvents {
	    @Subscribe
        public void EnderPearlUse(PlayerUseItemEvent.Start e){
        	if (e.item.getItem() == Items.ender_pearl && Config.disableEnderPearls && e.isCancelable()){
        		e.setCanceled(true);
        	}
        }
}
