package alekso56.tkcontentmod.turtles;

import net.minecraft.world.World;
import dan200.computercraft.api.peripheral.IPeripheral;
import dan200.computercraft.api.peripheral.IPeripheralProvider;

public class uselesswrapper implements IPeripheralProvider
{
    @Override
    public IPeripheral getPeripheral(World world, int x, int y, int z, int side)
    {
        if(world.getTileEntity(x, y, z) instanceof IPeripheral)
        {
            return (IPeripheral) world.getTileEntity(x, y, z);
        }
        return null;
    }
}
