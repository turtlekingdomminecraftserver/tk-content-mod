package alekso56.tkcontentmod.turtles;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import alekso56.tkcontentmod.Config;
import alekso56.tkcontentmod.Destructionhandler;
import alekso56.tkcontentmod.tkcontentmod;
import cpw.mods.fml.common.network.NetworkRegistry.TargetPoint;
import cpw.mods.fml.common.registry.GameRegistry;
import dan200.computercraft.api.lua.ILuaContext;
import dan200.computercraft.api.lua.LuaException;
import dan200.computercraft.api.peripheral.IComputerAccess;
import dan200.computercraft.api.peripheral.IPeripheral;
import dan200.computercraft.api.turtle.ITurtleAccess;
import dan200.computercraft.api.turtle.TurtleSide;

public class TKTurtleIPeripheral implements IPeripheral {
	private ITurtleAccess turtle;
	private TurtleSide side;

	public TKTurtleIPeripheral(ITurtleAccess turtle, TurtleSide side) {
	    //gives access to turtle and turtleside in peripheral
		this.side = side;
		this.turtle = turtle;
	}

	@Override
	public String getType() {
		return "TKPeripheral";
	}

	@Override
	public String[] getMethodNames() {
		return new String[]{"py","selfDestruct","isNorth"};
	}

	@Override
	public Object[] callMethod(IComputerAccess computer, ILuaContext context,
			int method, Object[] arguments) throws LuaException,
			InterruptedException {
		// TODO
		switch (method) {
		case 0: {
			if(Config.python){
				throw new LuaException("Not Supported yet ;(");
			}else{
				throw new LuaException("py is disabled ;(");
			}
		}
		case 1: {
			if(!turtle.getWorld().isRemote && Config.turtleselfdestruct && Config.selfdestructFuelRequirement <= turtle.getFuelLevel()){
				turtle.consumeFuel(Config.selfdestructFuelRequirement);
				if(!Config.noParticlesSelfdestruct){
					tkcontentmod.field.sendToAllAround(new Destructionhandler(turtle.getPosition().posX,  turtle.getPosition().posY,  turtle.getPosition().posZ)
					,new TargetPoint(turtle.getWorld().provider.dimensionId, turtle.getPosition().posX,  turtle.getPosition().posY,  turtle.getPosition().posZ, 40));
					if(!Config.blockFriendlySelfdestruct){
						Thread destructionSequence = new Thread() {
							@Override
							public void run() {
								try {
									Thread.sleep(2000);
									tkcontentmod.field.sendToAllAround(new Destructionhandler(turtle.getPosition().posX,  turtle.getPosition().posY,  turtle.getPosition().posZ)
									,new TargetPoint(turtle.getWorld().provider.dimensionId, turtle.getPosition().posX,  turtle.getPosition().posY,  turtle.getPosition().posZ, 40));
									Thread.sleep(3000);
									//add turtle destruction logic here                       false equals isSmoking
								    turtle.getWorld().createExplosion(null, turtle.getPosition().posX,turtle.getPosition().posY,turtle.getPosition().posZ, (float) (Math.E *Math.log10(Math.floor(turtle.getFuelLevel() /20))), true);
								    if (turtle != null){
								    	destroyAndDropTurtle();
								    }
								} catch(InterruptedException v) {}
							}  
						};
						destructionSequence.start();
						return null;
					}
				}
				destroyAndDropTurtle();
				return null;

			}else if(Config.selfdestructFuelRequirement >= turtle.getFuelLevel() && Config.turtleselfdestruct){
				throw new LuaException("Can't selfdestruct, because no fuel. I need "+(Config.selfdestructFuelRequirement-turtle.getFuelLevel())+" more..");
			}else{
				throw new LuaException("Selfdestruct disabled ;P");
			}
		}
		case 2: {
			if(turtle.getDirection() == 2){
				return new Object[]{true};
			}
			return new Object[]{false};
		}
		}
		return null;
	}

	private void destroyAndDropTurtle() {
		IInventory turtleinv = turtle.getInventory();
		EntityItem dropi;
		for (int j = 0; j < turtleinv.getSizeInventory(); ++j) {
			ItemStack itemstack = turtleinv.getStackInSlot(j);

			if (itemstack != null) {
				dropi = new EntityItem(turtle.getWorld(),turtle.getPosition().posX,turtle.getPosition().posY,turtle.getPosition().posZ,itemstack);
				turtleinv.setInventorySlotContents(j, null);
				turtle.getWorld().spawnEntityInWorld(dropi);
			}
		}
		turtleinv.closeInventory();
		TileEntity tortl = turtle.getWorld().getTileEntity(turtle.getPosition().posX, turtle.getPosition().posY, turtle.getPosition().posZ);
		NBTTagCompound nbt = new NBTTagCompound();
		if(tortl.getClass().getSimpleName().equals("TileTurtleAdvanced")){
			ItemStack advturtle = GameRegistry.findItemStack("ComputerCraft", "CC-TurtleAdvanced", 1);
			tortl.writeToNBT(nbt);
			advturtle.setTagCompound(nbt);
			dropi = new EntityItem(turtle.getWorld(),turtle.getPosition().posX,turtle.getPosition().posY,turtle.getPosition().posZ,advturtle);
			turtle.getWorld().spawnEntityInWorld(dropi);
		}
		else if(tortl.getClass().getSimpleName().equals("TileTurtleExpanded")){
			ItemStack normturtle = GameRegistry.findItemStack("ComputerCraft", "CC-TurtleExpanded", 1);
			tortl.writeToNBT(nbt);
			normturtle.setTagCompound(nbt);
			dropi = new EntityItem(turtle.getWorld(),turtle.getPosition().posX,turtle.getPosition().posY,turtle.getPosition().posZ,normturtle);
			turtle.getWorld().spawnEntityInWorld(dropi);
		}
		turtle.getWorld().func_147480_a(turtle.getPosition().posX, turtle.getPosition().posY, turtle.getPosition().posZ, false);
	}

	@Override
	public void attach(IComputerAccess computer) {}

	@Override
	public void detach(IComputerAccess computer) {}

	@Override
	public boolean equals(IPeripheral other) {
		return (other == this);
	}

}
