package alekso56.tkcontentmod;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.oredict.RecipeSorter;
import net.minecraftforge.oredict.RecipeSorter.Category;
import alekso56.tkcontentmod.CCRecipes.Computercrafting;
import alekso56.tkcontentmod.CCRecipes.Monicrafting;
import alekso56.tkcontentmod.CCRecipes.Pocketcrafting;
import alekso56.tkcontentmod.CCRecipes.Turtlecrafting;
import alekso56.tkcontentmod.blocks.EjectorRail;
import alekso56.tkcontentmod.blocks.LauncherBlock;
import alekso56.tkcontentmod.tilentities.TLauncherBlock;
import alekso56.tkcontentmod.turtles.TKTurtlePeripheral;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@Mod(modid = "tkcontent", name = "TK-CONTENT MOD", version = "1.7.1.3", dependencies = "required-after:Forge@[10.12.0.967,]")
public class tkcontentmod {
	@SidedProxy(clientSide="alekso56.tkcontentmod.ClientSide", serverSide="alekso56.tkcontentmod.ServerSide")
	public static ServerSide proxy;
	public static SimpleNetworkWrapper field;
	protected static List<String> bookPages = new ArrayList<String>();
	
	   public static CreativeTabs TKTab = new CreativeTabs("TurtleKingdom") {
			@Override
			public Item getTabIconItem() {
				return new ItemStack(Items.baked_potato,1,0).getItem();
			}
			 @Override
			@SideOnly(Side.CLIENT)
			public String getTranslatedTabLabel()
			{
			  return this.getTabLabel();
			}
			
			@Override
			public void displayAllReleventItems(List list) {
				super.displayAllReleventItems(list);
				if(Loader.isModLoaded("ComputerCraft")){
				 CCReference.addUpgrades(list);
				}
			}
		};
	
	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		Configuration config = new Configuration(event.getSuggestedConfigurationFile());
		Config.loadConfig(config);
		GameRegistry.registerBlock(new EjectorRail(), "EjectorRail");
		if(Loader.isModLoaded("ComputerCraft")){
		GameRegistry.registerBlock(new LauncherBlock(), "LauncherBlock");
		GameRegistry.registerTileEntity(TLauncherBlock.class, "TLauncher");
		}
		if(FMLCommonHandler.instance().getSide() == Side.CLIENT){
         initPages();
		}
        field = NetworkRegistry.INSTANCE.newSimpleChannel("Particle");
        field.registerMessage(Destructionhandler.ServerHandler.class, Destructionhandler.class, 0, Side.CLIENT);
	}
	
	@Mod.EventHandler
	public void Init(FMLInitializationEvent event){
		proxy.RegisterEvents();
		if(Loader.isModLoaded("ComputerCraft")){
			CCReference.Init();
			CCReference.upgrades.add(new TKTurtlePeripheral());
		    if(Config.ExtraRecipesCC){
		     GameRegistry.addRecipe(new Computercrafting());
		     GameRegistry.addRecipe(new Pocketcrafting());
		     GameRegistry.addRecipe(new Turtlecrafting());
		     GameRegistry.addRecipe(new Monicrafting());
		     RecipeSorter.register("ComputerCraft:CC-Computer", Computercrafting.class, Category.SHAPED, "after:minecraft:shaped");
		     RecipeSorter.register("ComputerCraft:pocketComputer", Pocketcrafting.class, Category.SHAPED, "after:minecraft:shaped");
		     RecipeSorter.register("ComputerCraft:CC-TurtleAdvanced", Turtlecrafting.class, Category.SHAPED, "after:minecraft:shaped");
		     RecipeSorter.register("ComputerCraft:CC-Peripheral", Monicrafting.class, Category.SHAPED, "after:minecraft:shaped");
		    }
		    ItemStack cabl = GameRegistry.findItemStack("ComputerCraft", "CC-Cable", 1);
		    cabl.setItemDamage(1);
		  GameRegistry.addRecipe(GameRegistry.findItemStack("tkcontent", "LauncherBlock", 1),new Object[]{ "iip", "mrp", "iip",
				  'i',Items.iron_ingot,'p', Blocks.piston, 'm', cabl,'r',Items.redstone});
		}
		GameRegistry.addShapelessRecipe(GameRegistry.findItemStack("tkcontent", "EjectorRail", 1),Blocks.rail,Blocks.piston);
	}
	
    protected static void toPages(InputStream inputStream){
    	try{
    		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
    		String line = br.readLine();
    		List<String> textfile = new ArrayList<String>();
    		while (line != null) {
    			textfile.add(line+"\n");
    			line = br.readLine();
    		}
    		br.close();
    		StringBuilder blah = new StringBuilder();
    		for(int i=0;i < textfile.size();i++){
    			blah.append(textfile.get(i));
    			if (i ==15 || i == 15*2 || i == 15*3 || i == 15*4 || i == 15*5 || textfile.size()-1 <= i){
    				tkcontentmod.bookPages.add(blah.toString());
    				blah = new StringBuilder();
    			}
    		}
    	} catch (IOException e) {
    		System.out.println("Failed reading buildchanges.txt, defaulting to fail");
    		tkcontentmod.bookPages.add("Failed reading text");
    	}
    }
    
    protected static void prepDefault(File f){
		try {
			 InputStream ips = new FileInputStream(f);
	         toPages(ips);
			} catch (FileNotFoundException e) {}
    }
    
	protected static void initPages(){
		File f = new File("./config/buildchanges.txt");
		if(!f.isFile()) {
		PrintWriter writer;
		try {
		String bs = Character.toString('\u00A7');
		writer = new PrintWriter("./config/buildchanges.txt", "Cp1252");
		writer.println(bs+"nMinecraft Formatting");
		writer.println(" ");
		writer.println(bs+"r"+bs+"00 "+bs+"11 "+bs+"22 "+bs+"33");
		writer.println(bs+"44 "+bs+"55 "+bs+"66 "+bs+"77");
		writer.println(bs+"88 "+bs+"99 "+bs+"aa "+bs+"bb");
		writer.println(bs+"cc "+bs+"dd "+bs+"ee "+bs+"ff");
		writer.println(" ");
		writer.println(bs+"r"+bs+"0k "+bs+"kMinecraft");
		writer.println(bs+"rl "+bs+"lMinecraft");
		writer.println(bs+"rm "+bs+"mMinecraft");
		writer.println(bs+"rn "+bs+"nMinecraft");
		writer.println(bs+"ro "+bs+"oMinecraft");
		writer.println(bs+"rr "+bs+"rMinecraft");
		writer.println("line 14");
		writer.println("line 15");
		writer.println("line 16");
		writer.println("line 17");
		writer.println("line 18");
		writer.close();
		} catch (FileNotFoundException e) {
		} catch (UnsupportedEncodingException e) {}
		}	
		
		if(tkcontentmod.bookPages.size() == 0 && !Config.FetchFromInternet){
          prepDefault(f);
		}
        else if (Config.FetchFromInternet && !Config.bookURL.isEmpty()){
			if (!Config.bookURL.contains("http://") && !Config.bookURL.contains("https://")){
			    Config.bookURL = "http://" + Config.bookURL;
			}

			URL url;
			try {
			    url = new URL(Config.bookURL);
			    toPages(url.openStream());
			} catch (MalformedURLException e) {
			    System.out.println("Bad url entered, defaulting to buildchanges.txt");
			    prepDefault(f);
			} catch (IOException e) {
				System.out.println("Failed fetching book from url, defaulting to buildchanges.txt");
				prepDefault(f);
			}
		}
    }
}
