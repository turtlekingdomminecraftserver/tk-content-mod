package alekso56.tkcontentmod.tilentities;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.play.server.S12PacketEntityVelocity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import alekso56.tkcontentmod.Config;
import dan200.computercraft.api.lua.ILuaContext;
import dan200.computercraft.api.lua.LuaException;
import dan200.computercraft.api.peripheral.IComputerAccess;
import dan200.computercraft.api.peripheral.IPeripheral;

public class TLauncherBlock extends TileEntity implements IPeripheral {
    private ArrayList<IComputerAccess> comp = new ArrayList<IComputerAccess>();
    public boolean AutoLaunch;
    public Double launchdistance;
    private Sides currentSide;
    
    public TLauncherBlock(){
    	AutoLaunch = Config.AutoLaunch;
    	launchdistance = 2.0;
    }
    
    private Sides identifySide(){
        int Metadata = worldObj.getBlockMetadata(xCoord, yCoord, zCoord)-1;
        if(Metadata == -1){return Sides.TOP;}
        return Sides.values()[Metadata];
    }
    
    @Override
    public String getType() {
        //peripheral.getType returns whaaaaat?
        return "LauncherBlock";
    }

    private static enum commands{
        launch, setLDistance,getLDistance,tAutoLaunch
    }
    
    private static enum Sides {
        TOP,NORTH, SOUTH, WEST, EAST
    };

    @Override
    public String[] getMethodNames() {
        // list commands you want..
        return new String[]{commands.launch.toString(),commands.setLDistance.toString(),commands.tAutoLaunch.toString()};
    }
    
    private AxisAlignedBB getBox(){
        if(currentSide == null){currentSide = identifySide();}
        AxisAlignedBB AABB = null;
        switch(currentSide){
        case EAST:
            AABB = AxisAlignedBB.getBoundingBox(xCoord + 1, yCoord, zCoord, xCoord + 2, yCoord + 1, zCoord + 1);
            break;
        case NORTH:
            AABB = AxisAlignedBB.getBoundingBox(xCoord, yCoord, zCoord - 1, xCoord + 1, yCoord + 1, zCoord);
            break;
        case SOUTH:
            AABB = AxisAlignedBB.getBoundingBox(xCoord, yCoord, zCoord + 1, xCoord + 1, yCoord + 1, zCoord + 2);
            break;
        case TOP:
            AABB = AxisAlignedBB.getBoundingBox(xCoord, yCoord + 1, zCoord, xCoord + 1, yCoord + 3, zCoord + 1);
            break;
        case WEST:
            AABB = AxisAlignedBB.getBoundingBox(xCoord - 1, yCoord, zCoord, xCoord, yCoord + 1, zCoord + 1);
            break;
        }
        return AABB;
    }
    
    public List<Entity> getEntitiesOnCurrentSide() {
        List<Entity> entities = worldObj.getEntitiesWithinAABB(Entity.class, getBox());
        if (entities.isEmpty()) {
            return null;
        } else {
            return entities;
        }
    }
    
    public Object[] launchResult(){
        List<Entity> Entities = getEntitiesOnCurrentSide();
        if(Entities != null && !Entities.isEmpty()){
        for(Entity pl : Entities){
            pl.fallDistance = -Short.MAX_VALUE;
            switch(currentSide){
            case EAST:
                pl.motionX = launchdistance;
                break;
            case NORTH:
                pl.motionZ = -launchdistance;
                break;
            case SOUTH:
                pl.motionZ = launchdistance;
                break;
            case TOP:
                pl.motionY = launchdistance;
                break;
            case WEST:
                pl.motionX = -launchdistance;
                break;
            }
            if(pl instanceof EntityPlayerMP)
            ((EntityPlayerMP) pl).playerNetServerHandler.sendPacket(new S12PacketEntityVelocity(pl));
        }
        return new Object[]{true};
        }
        return new Object[]{false};
    }
    
    @Override
    public void readFromNBT(NBTTagCompound nbt)
    {
        super.readFromNBT(nbt);
        AutoLaunch = nbt.getBoolean("autolaunch");
        launchdistance = nbt.getDouble("launchd");
    }

    @Override
    public void writeToNBT(NBTTagCompound nbt)
    {
        super.writeToNBT(nbt);
        nbt.setBoolean("autolaunch", AutoLaunch);
        nbt.setDouble("launchd", launchdistance);
    }

    @Override
    public Object[] callMethod(IComputerAccess computer, ILuaContext context,
            int method, Object[] arguments) throws LuaException,
            InterruptedException {
        // method is command
        switch(commands.values()[method]){
        case launch:
            return launchResult();
        case setLDistance:
            if(arguments.length == 1){
            	Double launchd = (Double) arguments[0];
                if(launchd != null){
                if(launchd <= 64 && launchd >= 1.0){
                 launchdistance = launchd;
                 return new Object[]{true};
                 }
                }
            }
            return new Object[]{false};
        case tAutoLaunch:
            if(AutoLaunch){
                AutoLaunch = false;
                return new Object[]{false};
            }else{
                AutoLaunch = true;
                return new Object[]{true};
            }
		case getLDistance:
			return new Object[]{launchdistance};
        }
        return null;
    }

    @Override
    public void attach(IComputerAccess computer) {
        comp.add(computer);
    }

    @Override
    public void detach(IComputerAccess computer) {
        comp.remove(computer);
    }
    
    public void SendEventToAll(String event){
        for (IComputerAccess c : comp) {
            c.queueEvent(event, new Object[]{true});
            //sends that event to all computers attached
        }
    }
    
    @Override
    public void updateEntity() {
        if(!worldObj.isRemote && AutoLaunch){
         launchResult();
        }
    }

    @Override
    public boolean equals(IPeripheral other) {
        if(other.getType() == getType()){return true;}
        else return false;
    }

}