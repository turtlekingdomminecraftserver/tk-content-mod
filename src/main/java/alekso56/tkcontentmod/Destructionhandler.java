package alekso56.tkcontentmod;

import io.netty.buffer.ByteBuf;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class Destructionhandler implements IMessage{
    public int posx,posy,posz;
    
    public Destructionhandler(){}
    
	public Destructionhandler(int posX, int posY, int posZ) {
		this.posx = posX;
		this.posy = posY;
		this.posz = posZ;
	}
	@Override
	public void fromBytes(ByteBuf buf) {
		this.posx = buf.readInt();
		this.posy = buf.readInt();
		this.posz = buf.readInt();
		tkcontentmod.proxy.spawnTurtleSelfDestruct(posx,posy,posz);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(this.posx);
		buf.writeInt(this.posy);
		buf.writeInt(this.posz);
	}
	
	public static class ServerHandler implements IMessageHandler<Destructionhandler, IMessage>{

		@Override
		public IMessage onMessage(Destructionhandler message, MessageContext ctx) {
			return null;
		}
		
	}
  
}
