package alekso56.tkcontentmod.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockRailBase;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import alekso56.tkcontentmod.tkcontentmod;

public class EjectorRail extends BlockRailBase
{
  IIcon[] icon;

  public EjectorRail()
  {
    super(true);
    this.setStepSound(soundTypeMetal);
    this.setCreativeTab(tkcontentmod.TKTab);
    this.setBlockName("EjectorRail");
  }

  @Override
public void registerBlockIcons(IIconRegister reg)
  {
	this.blockIcon = reg.registerIcon(Blocks.activator_rail.getIcon(0, 0).getIconName());
    this.icon = new IIcon[2];
    this.icon[0] = reg.registerIcon(Blocks.activator_rail.getIcon(0, 0).getIconName());
    this.icon[1] = reg.registerIcon(Blocks.activator_rail.getIcon(0, 0).getIconName() + "_powered");
  }

  @Override
public void onNeighborBlockChange(World world, int x, int y, int z, Block p_149695_5_)
  {
    if (world.isBlockIndirectlyGettingPowered(x, y, z))
    {
      int currentmeta = world.getBlockMetadata(x, y, z);
      if (currentmeta < 8)
        world.setBlockMetadataWithNotify(x, y, z, currentmeta + 8, 3);
    }
    else
    {
      int currentmeta = world.getBlockMetadata(x, y, z);
      if (currentmeta > 7) {
        world.setBlockMetadataWithNotify(x, y, z, currentmeta - 8, 3);
      }
    }

    super.onNeighborBlockChange(world, x, y, z, p_149695_5_);
  }

  @Override
public IIcon getIcon(int par1, int par2)
  {
    if (par2 > 7)
    {
      return this.icon[1];
    }

    return this.icon[0];
  }

  public Item getitemDropped(int metadata, Random random, int fortune)
  {
    return Item.getItemFromBlock(Block.getBlockFromName(getUnlocalizedName()));
  }
  
  @Override
  public void onMinecartPass(World world, EntityMinecart cart, int x, int y, int z)
  {
    if ((cart.riddenByEntity != null) && (world.isBlockIndirectlyGettingPowered(x, y, z))) {
      Entity rider = cart.riddenByEntity;
      rider.mountEntity(null);
      if (cart.motionX > 0.0D) {
        rider.moveEntity(-2.0D, 0.0D, -1.0D);
      }
      if (cart.motionX < 0.0D) {
        rider.moveEntity(2.0D, 0.0D, 1.0D);
      }
      if (cart.motionZ > 0.0D) {
        rider.moveEntity(1.0D, 0.0D, -2.0D);
      }
      if (cart.motionZ < 0.0D)
        rider.moveEntity(-1.0D, 0.0D, 2.0D);
    }
  }
}