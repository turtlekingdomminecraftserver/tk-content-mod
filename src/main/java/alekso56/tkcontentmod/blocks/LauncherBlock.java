package alekso56.tkcontentmod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import alekso56.tkcontentmod.tkcontentmod;
import alekso56.tkcontentmod.tilentities.TLauncherBlock;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class LauncherBlock extends Block {
    
    @SideOnly(Side.CLIENT)
    protected IIcon blockIconTop,blockIconBottom,blockIconLeft,blockIconRight,blockIconDown;

    public LauncherBlock() {
        super(new Material(MapColor.grayColor));
        this.setCreativeTab(tkcontentmod.TKTab);
        this.setHardness(0.6F).setStepSound(soundTypeMetal);
        this.setBlockName("LauncherBlock");
    }
    
    @Override
    public void registerBlockIcons(IIconRegister reg)
    {
      this.blockIcon = reg.registerIcon("tkcontent:LauncherBlockArrowUp");
      this.blockIconDown = reg.registerIcon("tkcontent:LauncherBlockArrowDown");
      this.blockIconLeft = reg.registerIcon("tkcontent:LauncherBlockArrowLeft");
      this.blockIconRight = reg.registerIcon("tkcontent:LauncherBlockArrowRight");
      this.blockIconTop = reg.registerIcon("tkcontent:LauncherBlockTop");
      this.blockIconBottom = reg.registerIcon("tkcontent:LauncherBlockBottom");
    }
    
    @Override
    public boolean hasTileEntity(int meta) {
        return true;
    }

    @Override
    public TileEntity createTileEntity(World world, int meta) {
        return new TLauncherBlock();
    }
    
    @Override
    public void onBlockPlacedBy(World world, int par2, int par3, int par4,
            EntityLivingBase par5EntityLivingBase, ItemStack par6ItemStack) {
        if(!world.isRemote){
            int l = MathHelper.floor_double(par5EntityLivingBase.rotationYaw * 4.0F / 360.0F + 2.5D) & 3;
            if(par5EntityLivingBase.rotationPitch >= 62){
                world.setBlockMetadataWithNotify(par2, par3, par4, 1,2);
                return;
            }
            if (l == 0) {
                world.setBlockMetadataWithNotify(par2, par3, par4, 3,2);
                return;
            }
            if (l == 1) {
                world.setBlockMetadataWithNotify(par2, par3, par4, 4,2);
                return;
            }
            if (l == 2) {
                world.setBlockMetadataWithNotify(par2, par3, par4, 2,2);
                return;
            }
            if (l == 3) {
                world.setBlockMetadataWithNotify(par2, par3, par4, 5,2);
                return;
            }
            
        }
    }
    
    /**
     * Called whenever an entity is walking on top of this block. Args: world, x, y, z, entity
     */
    @Override
    public void onEntityWalking(World world, int x, int y, int z, Entity ent) {
        if(world.getBlockMetadata(x, y, z) == 1){
            TileEntity tile = world.getTileEntity(x, y, z);
            if (tile instanceof TLauncherBlock && tile != null){
                TLauncherBlock tilet = (TLauncherBlock) tile;
                if(!world.isRemote){
                 tilet.SendEventToAll("entity");
                }
            }
        }
    }
    
    @SideOnly(Side.CLIENT)
    @Override
    public IIcon getIcon(int side, int metadata)
    {
        if (metadata == side && metadata != 0) {
            return blockIconTop;
        } else if(side == 0){
            return blockIconBottom;
        } else if(side == 4 && metadata == 2){
        	return blockIconLeft;
        }  else if(side == 1 && metadata == 2){
        	return blockIcon;
        }  else if(side == 5 && metadata == 2){
        	return blockIconRight;
        } else if(side == 5 && metadata == 3){
        	return blockIconLeft;
        } else if(side == 4 && metadata == 3){
        	return blockIconRight;
        } else if(side == 1 && metadata == 3){
        	return blockIconDown;
        } else if(side == 2 && metadata == 4){
        	return blockIconRight;
        } else if(side == 3 && metadata == 4){
        	return blockIconLeft;
        } else if(side == 2 && metadata == 5){
        	return blockIconLeft;
        } else if(side == 3 && metadata == 5){
        	return blockIconRight;
        } else if(side == 1 && metadata == 5){
        	return blockIconRight;
        } 
        else if(side == 4 && metadata == 0){
        	return blockIconTop;
        } else if(side == 1 || side == 3 && metadata == 0){
        	return blockIconLeft;
        } 
        else {
            return blockIcon;
        }
    }

}
