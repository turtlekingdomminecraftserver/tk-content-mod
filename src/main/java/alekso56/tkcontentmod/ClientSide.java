package alekso56.tkcontentmod;

import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.EntityHugeExplodeFX;
import alekso56.tkcontentmod.turtles.turtleSelfDestructSequence;
import cpw.mods.fml.common.FMLCommonHandler;

public class ClientSide extends ServerSide {
	
  @Override
  public void RegisterEvents(){
	  FMLCommonHandler.instance().bus().register(new ClientEvents());
	  
  }
  
  @Override
  public void spawnTurtleSelfDestruct(final int posx,final int posy,final int posz){
  final float[][] colors = {
			{ 1F / 0xFF * 0x9A, 1F / 0xFF * 0xFE, 1F / 0xFF * 0x2E },
			{ 1F / 0xFF * 0x01, 1F / 0xFF * 0xFF, 1F / 0xFF * 0xFF },
			{ 1F / 0xFF * 0x08, 1F / 0xFF * 0x08, 1F / 0xFF * 0x8A }
		};
		
		final float size = (float) (1.75D * 1.75D);
		final int pieces = (int) Math.max(Math.min((size * 64), 128), 8);
		for (int i = 0; i < pieces; i++) {
			final float[] color = colors[i % 3];
			
          final turtleSelfDestructSequence selfdestruct = new turtleSelfDestructSequence(
					Minecraft.getMinecraft().theWorld,
					posx+0.5, posy+2, posz+0.5,
					color[0], color[1], color[2]
			);
          //name,volume,pitch
          Minecraft.getMinecraft().thePlayer.playSound("tkcontent:turtlebomb", 1, 1);
          Thread one = new Thread() {
        	    @Override
				public void run() {
        	        try {
        	        	Minecraft.getMinecraft().effectRenderer.addEffect(selfdestruct);
        	            Thread.sleep(3000);
        	            Minecraft.getMinecraft().effectRenderer.addEffect(new EntityHugeExplodeFX(Minecraft.getMinecraft().theWorld,posx,posy,posz,color[0],color[1],color[2]));
        	        } catch(InterruptedException v) {}
        	    }  
        	};
         one.start();
		}
  }
}
