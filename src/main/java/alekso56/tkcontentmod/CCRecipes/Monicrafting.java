package alekso56.tkcontentmod.CCRecipes;

import net.minecraft.init.Items;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.world.World;
import cpw.mods.fml.common.registry.GameRegistry;

public class Monicrafting  implements IRecipe{

	@Override
	public boolean matches(InventoryCrafting inv, World world){
		byte goldingots = 0;
		boolean hasMonitor = false;
		for (int j = 0; j < inv.getSizeInventory(); j++) {
			if (inv.getStackInSlot(j) != null) {
				if (j == 4 && inv.getStackInSlot(j).getItem() == GameRegistry.findItemStack("ComputerCraft", "CC-Peripheral", 1).getItem() && inv.getStackInSlot(j).getItemDamage() == 2) {
					hasMonitor = true;
				}else if(j != 4 && inv.getStackInSlot(j).getItem() == Items.gold_ingot){
					goldingots++;
				}else{break;}
			}else{break;}
		}
		if(goldingots == 8 && hasMonitor){
		    return true;
		}
		return false;
	}

	@Override
	public ItemStack getCraftingResult(InventoryCrafting inv) {
		ItemStack Result = inv.getStackInSlot(4).copy();
		Result.setItemDamage(4);
		return Result;
	}

	@Override
	public int getRecipeSize() {
		return 9;
	}

	@Override
	public ItemStack getRecipeOutput() {
		ItemStack Result = GameRegistry.findItemStack("ComputerCraft", "CC-Peripheral", 1);
		Result.setItemDamage(4);
		return Result;
	}
}

