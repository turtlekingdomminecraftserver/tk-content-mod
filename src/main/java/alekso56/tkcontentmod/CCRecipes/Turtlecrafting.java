package alekso56.tkcontentmod.CCRecipes;

import net.minecraft.init.Items;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import cpw.mods.fml.common.registry.GameRegistry;

public class Turtlecrafting implements IRecipe{

	@Override
	public boolean matches(InventoryCrafting inv, World world) {
		byte goldingots = 0;
		boolean hasTurtlePower = false;
		for (int j = 0; j < inv.getSizeInventory(); j++) {
			if (inv.getStackInSlot(j) != null) {
				if (j == 4 && inv.getStackInSlot(j).getItem() == GameRegistry.findItemStack("ComputerCraft", "CC-Turtle", 1).getItem() || inv.getStackInSlot(j).getItem() == GameRegistry.findItemStack("ComputerCraft", "CC-TurtleExpanded", 1).getItem()) {
					hasTurtlePower = true;
				}else if(j != 4 && inv.getStackInSlot(j).getItem() == Items.gold_ingot){
					goldingots++;
				}else{break;}
			}else{break;}
		}
		if(goldingots == 8 && hasTurtlePower){
		    return true;
		}
		return false;
	}

	@Override
	public ItemStack getCraftingResult(InventoryCrafting inv) {
		ItemStack treatme = inv.getStackInSlot(4).copy();
		ItemStack Result = GameRegistry.findItemStack("ComputerCraft", "CC-TurtleAdvanced", 1);
		
		NBTTagCompound nbt = new NBTTagCompound();
		NBTTagCompound nbt2 = new NBTTagCompound();
		treatme.writeToNBT(nbt);
		Result.writeToNBT(nbt2);
		nbt2.removeTag("tag");
		
		NBTTagCompound InnerOutputNBT = nbt.getCompoundTag("tag");
		if(InnerOutputNBT != null){
		 if((treatme.getItemDamageForDisplay() & 1) == 1){
			 InnerOutputNBT.removeTag("leftUpgrade");
			 InnerOutputNBT.setShort("leftUpgrade", (short) 5);
		 }
		 // isMiningTurtleFlag                                           isNormalTurtleFlag
		 if((treatme.getItemDamageForDisplay() & 1) == 1 || InnerOutputNBT.getShort("leftUpgrade") == 0){
		  InnerOutputNBT.setInteger("computerID", (treatme.getItemDamageForDisplay() >> 2)-1);
		 }
		 nbt2.setTag("tag", InnerOutputNBT);
          

		 Result.readFromNBT(nbt2);
		}
		Result.setStackDisplayName(treatme.getDisplayName());
		return Result;
	}

	@Override
	public int getRecipeSize() {
		return 9;
	}

	@Override
	public ItemStack getRecipeOutput() {
		return GameRegistry.findItemStack("ComputerCraft", "CC-TurtleAdvanced", 1);
	}
	
}
