package alekso56.tkcontentmod.CCRecipes;

import net.minecraft.init.Items;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.world.World;
import cpw.mods.fml.common.registry.GameRegistry;

public class Pocketcrafting implements IRecipe{

	@Override
	public boolean matches(InventoryCrafting inv, World world) {
		byte goldingots = 0;
		boolean hasPocketComputer = false;
		for (int j = 0; j < inv.getSizeInventory(); j++) {
			if (inv.getStackInSlot(j) != null) {
				if (j == 4 && inv.getStackInSlot(j).getItem() == GameRegistry.findItemStack("ComputerCraft", "pocketComputer", 1).getItem() && inv.getStackInSlot(j).getItemDamageForDisplay() == 0) {
					hasPocketComputer = true;
				}else if(j != 4 && inv.getStackInSlot(j).getItem() == Items.gold_ingot){
					goldingots++;
				}else{break;}
			}else{break;}
		}
		if(goldingots == 8 && hasPocketComputer){
		    return true;
		}
		return false;
	}

	@Override
	public ItemStack getCraftingResult(InventoryCrafting inv) {
		ItemStack Result = inv.getStackInSlot(4).copy();
		Result.setItemDamage(1);
		return Result;
	}

	@Override
	public int getRecipeSize() {
		return 9;
	}

	@Override
	public ItemStack getRecipeOutput() {
		ItemStack out = GameRegistry.findItemStack("ComputerCraft", "pocketComputer", 1);
		out.setItemDamage(1);
		return out;
	}
	
}
